﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace OS_BarberShop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }


        ///////////////////////////////////////////////////////////////////////////////

        int NumOfCustomers = 50;
        int NumOfBarbers = 3;
        int NumOfCashiers = 1;
        int MaxCapacity = 20;
        int CustomerCnt;
        string CustomerID = "Customer ♪ ♫ ";

        Semaphore c_inside, c_outside, C_Cnt;
        Semaphore sofa;
        Semaphore barber_chair;
        //The semaphore coord ensures that barbers perform only one task at a time.
        Semaphore coord;
        //The semaphore mutex1 protects access to the global variable count so that each customer receives a unique number
        //Semaphore mutex1, mutex2;
        Semaphore cust_ready;
        Semaphore leave_b_chair;
        Semaphore payment;
        Semaphore receipt;
        Semaphore[] Finish;
        Semaphore Capacity;
        Semaphore max_capacity;

        Thread[] TH;
        Thread[] Customers;
        Thread[] Barbers;
        Thread[] Cashiers;

        Queue<int> Q1 = new Queue<int> { };

        void Customer()
        {
            Random Rnd = new Random(System.Environment.TickCount);
            Random Rnd2;
            int Cnt;
            //give each customer unique ID
            C_Cnt.Wait();
            Cnt = CustomerCnt++;
            Rnd2 = new Random(System.Environment.TickCount);
            Thread.Sleep(Rnd.Next(30));
            C_Cnt.Signal();

            //[2]customer outside
            c_outside.Wait();
            listBox1.Items.Add("Customer" + Cnt.ToString());
            c_outside.Signal();

            Thread.Sleep(Rnd.Next(700));
            Capacity.Wait();

            //customer going to sofa(Remove from listBox1)
            c_outside.Wait();
            listBox1.Items.Remove("Customer" + Cnt.ToString());
            c_outside.Signal();
            //Customer has to wait in Queue to set on sofa
            sofa.Wait();
            Q1.Enqueue((int)Cnt);
            listBox3.Items.Add("Customer" + Cnt.ToString());
            cust_ready.Signal();
            sofa.Signal();

            //[3]customer goes to set on barber chair(must be removed from sofa first)
            sofa.Wait();
            Q1.Dequeue();
            listBox3.Items.Remove("Customer" + Cnt.ToString());
            cust_ready.Signal();
            sofa.Signal();
            //[3]customer goes to set on barber chair
            barber_chair.Wait();
            listBox4.Items.Add("Customer" + Cnt.ToString() + " Sat on Chair");
            barber_chair.Signal();

            //customer pays
            leave_b_chair.Wait();
            listBox4.Items.Remove("Customer" + Cnt.ToString() + " Sat on Chair");
            leave_b_chair.Signal();

            payment.Wait();
            listBox5.Items.Add("Customer" + Cnt.ToString() + " Pay");
            payment.Signal();

            //[4]customer finishes and pay
            Finish[Cnt - 1].Wait();

            //[5] Customer leaves
            Capacity.Signal();

            c_inside.Wait();
            listBox5.Items.Remove("Customer " + Cnt.ToString());
            c_inside.Signal();

            c_outside.Wait();
            listBox6.Items.Add("Customer " + Cnt.ToString());
            c_outside.Signal();
        }
        void Barber() { }
        void Cashier() { }

        //void customer()
        //{
        //    int customer_no;
        //    max_capacity.Wait();
        //    enter_shop();
        //    mutex1.Wait();
        //    customer_no = count;
        //    count++;
        //    mutex1.Signal();
        //    sofa.Wait();
        //    sit_on_sofa();
        //    barber_chair.Wait();
        //    get_up_from_sofa();
        //    sofa.Signal();
        //    sit_in_barber_chair();
        //    mutex2.Wait();
        //    enqueue(customer_no);
        //    cust_ready.Signal();
        //    mutex2.Signal();
        //    finish[customer_no].Wait();
        //    leave_barber_chair();
        //    leave_b_chair.Signal();
        //    pay();
        //    payment.Signal();
        //    receipt.Wait();
        //    exit_shop();
        //    max_capacity.Signal();


        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            start.Enabled = false;
            CustomerCnt = 1;

            listBox1.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();

            C_Cnt = new Semaphore(1);
            c_outside = new Semaphore(1);
            c_outside = new Semaphore(1);
            c_inside = new Semaphore(1);
            Capacity = new Semaphore(MaxCapacity);
            cust_ready = new Semaphore(0);
            Finish = new Semaphore[NumOfCustomers];
            for (int i = 0; i < NumOfCustomers; i++)
                Finish[i] = new Semaphore(0);


            Customers = new Thread[NumOfCustomers];
            Barbers = new Thread[NumOfBarbers];
            Cashiers = new Thread[NumOfCashiers];
            for (int i = 0; i < NumOfBarbers; i++)
            {
                TH = new ThreadStart(Barber);
                Barbers[i] = new Thread(TH);
                Barbers[i].Start();
            }

            for (int i = 0; i < NumOfCustomers; i++)
            {
                TH = new ThreadStart(Customer);
                Customers[i] = new Thread(TH);
                Customers[i].Start();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "Pause")
            {
                for (int i = 0; i < NumOfCustomers; i++)
                {
                    if (Customers[i].IsAlive)
                        Customers[i].Suspend();
                }
                for (int i = 0; i < NumOfBarbers; i++)
                {
                    if (Barbers[i].IsAlive)
                        Barbers[i].Suspend();
                }
                button2.Text = "Resume";
            }
            else
            {
                for (int i = 0; i < NumOfCustomers; i++)
                {
                    if (Customers[i].IsAlive)
                        Customers[i].Resume();
                }
                for (int i = 0; i < NumOfBarbers; i++)
                {
                    if (Barbers[i].IsAlive)
                        Barbers[i].Resume();
                }
                button2.Text = "Pause";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            start.Enabled = true;
            for (int i = 0; i < NumOfCustomers; i++)
                Customers[i].Abort();

            for (int i = 0; i < NumOfBarbers; i++)
                Barbers[i].Abort();
        }


    }
}
