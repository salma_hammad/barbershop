﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS_BarberShop
{
    class Fair
    {

        //Fair_Peseudocode:

        semaphore max_capacity = 20;
        semaphore sofa = 4;
        semaphore barber_chair = 3, coord = 3;
        semaphore mutex1 = 1, mutex2 = 1;
        semaphore cust_ready = 0, leave_b_chair = 0, payment= 0, receipt = 0;
        semaphore finished [50] = {0};
        int count;

        void customer ()
        {
            int custnr;
            semWait (max_capacity);
            enter_shop();
            semWait(mutex1);
            custnr = count;
            count++;
            semSignal(mutex1);
            semWait(sofa);
            sit_on_sofa();
            semWait(barber_chair);
            get_up_from_sofa();
            semWait(barber_chair);
            get_up_from_sofa(); 
            semSignal(sofa); 
            sit_in_barber_chair(); 
            semWait(mutex2);
            enqueue1(custnr);
            semSignal(cust_ready);
            semSignal(mutex2);
            semWait(finished[custnr]);
            leave_barber_chair();
            semSignal(leave_b_chair);
            pay();
            semSignal(payment);
            semWait(receipt);
            exit_shop();
            semSignal(max_capacity);
        }

        void barber()
        {
            int b_cust;
            while(true)
            {
                semWait(cust_ready);
                semWait(mutex2);
                dequeue1(b_cust);
                semSignal(mutex2);
                semWait(coord);
                cut_hait();
                semSignal(coord);
                semSignal(finished[b_cust]);
                semWait(leave_b_chair);
                semSignal(barber_chair);
            }
        }
        
        void cashier()
        {
            while(true)
            {
                semWait(payment);
                semWait(coord);
                accept_pay();
                semSignal(coord);
                semSignal(receipt);
            }
        }

        void main()
        {
            count = 0;
           //parbegin (customer,. ..50 times,. ..customer, barber, barber, barber, cashier);
        }
    }
}
